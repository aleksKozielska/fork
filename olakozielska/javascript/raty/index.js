var loan = {
    amount: 1000,
    months: 12,
    interest:0.10,
}
var payments = [];

var btn_count = document.getElementById("btn_count");

var zmienna = document.getElementById('loan_amount');
console.log(zmienna.value);
btn_count.addEventListener('click', function(){
    payments = [];
    loan.amount = document.getElementById('loan_amount').value;
    loan.months = document.getElementById('loan_months').value;
    loan.interest = document.getElementById('loan_interest').value;
    debugger;
    count(loan);
    console.table(payments);
    //printTableToHtml(payments);

})

function count (loan){
    var fund = loan.amount/loan.months;
    var mortgage = loan.amount;
    var interests = 0;
    for(let i=0; i<loan.months; i++){
    
        interests = loan.interest*(mortgage/12);
        var rate = fund + interests;
        payments.push({
            mortgage: mortgage,
            rate,
            fund: fund,
            interests: interests
        })
        mortgage -= fund;
    }
    console.table(payments);
}

function printTableToHtml (payments){
    var tbody = document.getElementById("tbody-main");
    tbody.innerHTML = '';
    var count = 0;
    for(item of payments){
        //debugger;
        var tr = document.createElement('tr');
        tr.innerHTML = ` <td> ${++count} </td>
        <td> ${item.fund.toFixed(2)} </td>
        <td> ${item.interests.toFixed(2)} </td>
        <td> ${item.mortgage.toFixed(2)} </td>
        <td>${item.rate.toFixed(2)}  </td>`
        tbody.appendChild(tr);

        //==========DIFFERENT WAY ===================
        // tbody.innerHTML += `<tr>
        // <td> ${++count} </td>
        // <td> ${item.fund.toFixed(2)} </td>
        // <td> ${item.interests.toFixed(2)} </td>
        // <td> ${item.mortgage.toFixed(2)} </td>
        // <td>${item.rate.toFixed(2)}  </td>
        //     </tr>`;
    }
}

count(loan);
printTableToHtml(payments);