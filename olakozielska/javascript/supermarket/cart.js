var cartItems = [
  {
    product_id: "123",
    product: products[0],
    amount: 2,
    subtotal: products[0].price * 2,
  },
];

var cartTotal = 20;
getCartTotalPrice(cartItems);
printCartItemsToHtml(cartItems);
printCartTotal();

function addToCart(product_id) {
  var alreadyInCart = false;

  for (let item of cartItems) {
    if (item.product_id === product_id) {
      item.amount++;
      item.subtotal += item.product.price;
      alreadyInCart = true;
      break;
    }
  }

  if (alreadyInCart === false) {
    var product = getProductById(product_id);
    cartItems.push({
      product_id: product_id,
      product: product,
      amount: 1,
      subtotal: product.price,
    });
  }

  getCartTotalPrice(cartItems);
  printCartItemsToHtml(cartItems);
  printCartTotal();
  console.log(cartItems);
  console.log(cartTotal);
}

function getProductById(product_id) {
  for (let item of products) {
    if (item.id === product_id) return item;
  }
}

function getCartTotalPrice(cartItems) {
  //debugger;
  cartTotal = 0;
  for (let item of cartItems) {
    cartTotal += item.subtotal;
  }
}

function printCartItemsToHtml(cartItems) {
  basketList.innerHTML = "";
  for (let item of cartItems) {
    basketList.innerHTML += printCartItem(item);
  }
}

function printCartTotal() {
  basketList.innerHTML += `  <li class="list-group-item d-flex justify-content-between">
    <span>Total (USD)</span>
    <strong id="total-price">$${cartTotal}</strong>
  </li>`;
}

function printCartItem(item) {
  var item_to_render = `<li class="list-group-item d-flex justify-content-between lh-sm" id="${item.product_id}">
    <div>
      <h6 class="my-0">${item.product.name}</h6>
      <small class="text-muted">${item.product.description}</small>
    </div>
      <span class="text-muted">${item.amount} + $${item.product.price} + $${item.subtotal}</span>
      <span class="close removeBtn">&times;</span>
    </li>`;
  return item_to_render;
}

var items = document.querySelectorAll(".js-cart-items .js-product");

for (let item of items) {
  var closeBtn = item.querySelector(".close");
  closeBtn.addEventListener("click", function (event) {
    console.log(item, event.target);
    item.remove();
  });
}
