var tax = 23
// Apply to PROMOTED products that don`t have their own discount
var shop_discount = 0.05;
 
var products = [
    {
      id: "123",
      name: "Banana Pancakes",
      price: 1690,
      description: "Best pancakes ever.",
      promotion: true,
      discount: 0,
      image:
        "https://images.unsplash.com/photo-1528207776546-365bb710ee93?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      date_added: new Date().toLocaleDateString("en-GB"),
      category: "pancakes",
    },
    {
      id: "124",
      name: "Chocolate cookies",
      price: 1090,
      description: "Best cookies ever.",
      promotion: false,
      discount: 0.75,
      image:
        "https://images.unsplash.com/photo-1528207776546-365bb710ee93?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      date_added: new Date().toLocaleDateString("en-GB"),
      category: "cookies",
    },
    {
      id: "125",
      name: "Donuts",
      price: 1390,
      description: "Best donuts ever.",
      promotion: true,
      discount: 0,
      image:
        "https://images.unsplash.com/photo-1528207776546-365bb710ee93?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      date_added: new Date().toLocaleDateString("en-GB"),
      category: "donuts",
    },
  ];
 
  var cartProducts = [
    {
      id: "123",
      name: "Banana Pancakes",
      price: 1690,
      description: "Best pancakes ever.",
      promotion: true,
      discount: 0,
      image:
        "https://images.unsplash.com/photo-1528207776546-365bb710ee93?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      date_added: new Date().toLocaleDateString("en-GB"),
      category: "pancakes",
    },
    {
      id: "124",
      name: "Chocolate cookies",
      price: 1090,
      description: "Best cookies ever.",
      promotion: false,
      discount: 0.75,
      image:
        "https://images.unsplash.com/photo-1528207776546-365bb710ee93?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      date_added: new Date().toLocaleDateString("en-GB"),
      category: "cookies",
    },
 
  ];
  
 
// ===== DIFFERENT WAYS OF ASSIGNING OBJECTS ========== 
var product1 = Object.assign({}, products[0]);
 
var product2 = { ...products[0] };
 
var product3 = JSON.parse(JSON.stringify(products[0]));
 
var product4 = {};
for (key in products[0]) {
  product4[key] = products[0][key];
}
// ===================================================
 
 
var filter_promotion = true;
var index = 0;
var limit = 2;
 
function renderProducts() {
  var count = 0;
  productList.innerHTML = ''
 
  for (let product of products) {
 
    if (filter_promotion == true && product.promotion == false) continue;
 
    if (count++ > limit) break;
 
    renderToDocument(product);
  }
 
}
function renderProductsToCart() {
    basketList.innerHTML = ''
    var count = 0;
    var totalPrice = getCartPrice(cartProducts);
    for (let product of cartProducts) {
        var nett_price = getDiscountedNettPrice(product);
        var gross_price = getGrossPrice(nett_price);
 
        var item = `<li class="list-group-item d-flex justify-content-between lh-sm" id="${product.id}">
        <div>
          <h6 class="my-0">${product.name}</h6>
          <small class="text-muted">${product.description}</small>
        </div>
          <span class="text-muted">$${gross_price}</span>
          <span class="close removeBtn">&times;</span>
        </li>`;
        //debugger;
        
        basketList.innerHTML += item
 
        if(++count === cartProducts.length){
            basketList.innerHTML += `  <li class="list-group-item d-flex justify-content-between">
            <span>Total (USD)</span>
            <strong id="total-price">$${totalPrice}</strong>
          </li>`;
        }
    }
    var deletes = document.querySelectorAll('.removeBtn')
    // Iterate all nodes
    deletes.forEach(btn => {
    // Attach event listener. Note to preserve the button this-reference
    // by using the non-shorthand function
    btn.addEventListener('click', function() {
        var li = this.parentNode
        console.log(li.id);
        cartProducts = cartProducts.filter(el => {
            return el.id !== li.id;
          })
        li.remove()
        var new_price = getCartPrice(cartProducts);
        var total_price_el = document.getElementById("total-price");
        total_price_el.innerHTML = `$${new_price}`;
    })
 
})
 
  }
 
function getCartPrice (basketProducts){
    var basketPrice = 0;
    for(let product of basketProducts){
        var nett_price = getDiscountedNettPrice(product);
        var gross_price = getGrossPrice(nett_price);
        basketPrice += gross_price;
    }
    return basketPrice;
}
 
function renderToDocument(product) {
  var nett_price = getDiscountedNettPrice(product);
  var gross_price = getGrossPrice(nett_price);
 
  var item = `<div class="list-group-item pt-3 pb-3">
  <div class="row">
      <div class="col-10">
          <h3>${product.name}</h3>
          <p>${product.description}</p>
      </div>
      <div class="col-2">
          <div>
              <h4 class="pb-2">  ${gross_price} </h4>
          </div>
          <button class="btn btn-secondary">Add to cart</button>
      </div>
  </div>
  </div>`;
  productList.innerHTML += item
}
  
 
 
function getDiscountedNettPrice(product) {
 
  if (product.discount) {
    return product.price * (1 - product.discount);
  } else return (product.price *= (1 - shop_discount));
 
}
 
function getGrossPrice(nett_price) {
    return (Math.round(nett_price * (1 + tax / 100)))/100;
  }
  
var promoted_btn = document.getElementById('promotion-btn');
promoted_btn.addEventListener('click', function(event){
  filter_promotion = event.target.checked;
  renderProducts();
})


renderProducts();
renderProductsToCart();