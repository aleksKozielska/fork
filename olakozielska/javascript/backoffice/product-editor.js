
class ProductEditEvent extends Event {

    constructor(type) {
      super(type);
    }
  }

class ProductEditorView extends EventTarget  {

    constructor(selector){
        super();
        this.el = document.querySelector(selector);
        this.form = document.querySelector('form');
        console.log(this.form.elements);

        this.form.addEventListener('submit', event => {
            event.preventDefault() // Stop form from reloading the page!

            const data = this.getData();
            console.log(data);
            fetch('http://localhost:3000/products/' + data['id'], {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
                }).then(res => res.json())
                .then(console.log)
            //console.log(this.getData())
          })
    }

    _data = {}

    setData(data){
        this._data = data;
        this.form.elements['name'].value = this._data['name'];  
        this.form.elements['price'].value = this._data['price'];    
    }
    getData(){
        this._data['name'] = this.form.elements['name'].value;
        this._data['price'] = parseFloat(this.form.elements['price'].value);
        return this._data
    }
}

// const f = new FormData(this.form)
// const data = Object.fromEntries(f.entries())
// console.log(data)

// this._data['name'] = this.form.elements['name'].value
// this._data['price_nett'] = parseFloat(this.form.elements['price'].value) * 100

// return this._data
