class AppController {

    productEditor = new ProductEditorView (".productList");
    listOfProducts = new Products(products, ".productList");


    constructor(){
        this.listOfProducts.renderProducts();
        this.listOfProducts.addEventListener('product_selected', (event)=>{
            
            const product = products.find(prod => prod.id == event.productId);
            app.productEditor.setData(product);
        })
    }
}

const app = new AppController();

//app.productEditor.setData(products[0]);