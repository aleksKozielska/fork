
class ProductEvent extends Event {

  constructor(type, productId) {
    super(type);
    this.productId = productId
  }
}

class Products extends EventTarget {
  constructor(products, productListClass) {
    super();
    this.productListHTML = document.querySelector(productListClass);
    this.products = products;
    this.filtered = products;
    this.productListHTML.addEventListener("click", event => {
      removeActiveClass();
      addActiveClass();
      var el = event.target.closest('[data-product-id]');
      const product_id = el.dataset['productId']
      loadJSON('http://localhost:3000/products/' + product_id).then(product => {
        app.productEditor.setData(product)
      })
      this.dispatchEvent(new ProductEvent('product_selected', product_id))
    })
  }

  renderToDocument(product) {
    var item = `<li class="list-group-item" data-product-id= ${product.id}>${product.name}</li>`;
    this.productListHTML.innerHTML += item;
  }

  renderProducts() {
    this.productListHTML.innerHTML = "";
    this.filtered.forEach(this.renderToDocument.bind(this));
  }

  //CALLBACK
  //   filter(){
  //     loadJSON("products.json", resp => {
  //       this.filtered = resp.filter(product =>
  //         product.price >10);
  //         this.renderProducts();
  //     })
  //   }


  // }
  // function loadJSON( url, callback){
  //   xhr = new XMLHttpRequest();
  //   xhr.open('GET', "products.json");
  //   xhr.addEventListener('loadend', event => {
  //     const data = JSON.parse(event.target.responseText);
  //     console.log(data);
  //     callback(data);
  //   });
  //   xhr.send();
  // }

  filter() {
    loadJSON("products.json").then(
      resp => {
        this.filtered = resp.filter(product =>
          product.price > 10);
        this.renderProducts();
      }
    )
  }
}

function loadJSON(url) {

  return new Promise((resolve) => {
    xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.addEventListener('loadend', event => {
      const data = JSON.parse(event.target.responseText);
      resolve(data)
    });
    xhr.send();
  })
}




function addActiveClass() {
  var el = event.target.closest('[data-product-id]');
  el.classList.add('green');

}

function removeActiveClass() {
  var list = event.target.closest('ul');
  for (var child of list.children) {
    child.classList.remove('green');
  }
}




