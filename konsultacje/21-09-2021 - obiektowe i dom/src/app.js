import { TaskListItemView2 } from "./TaskListItemView.js"
import { TasksCollection } from "./TasksCollection.js"
import { TasksView } from "./TasksView.js"


export class Application {

  tasksCollection = new TasksCollection([
    {
      "id": "123",
      "title": "Kup placki",
      "completed": true
    },
    {
      "id": "234",
      "title": "Kup Mleko",
      "completed": false
    }
  ])

  tasksView = new TasksView()
  tasksView2 = new TasksView(document.querySelector('#tasks-list2'))

  constructor() {

    this.tasksView.listenTo(this.tasksCollection)
    this.tasksView.render()

    this.tasksView2.listenTo(this.tasksCollection)
    this.tasksView2.itemFactory = TaskListItemView2
    this.tasksView2.render()


    document.querySelector('.add-task').addEventListener('click', () => {
      this.tasksCollection.reset([
        ...this.tasksCollection.getAll(),
        { id: '345', title: 'Nowy', completed: false }
      ])
    })


    /* Debug: */
    window.tasksView = this.tasksView
    window.tasks = this.tasksCollection
  }

  static start() {
    // this // undefined yet!
    return new Application()
  }
}

Application.start()