productsData = {
    items:[],
    view:null,
    addItem(item){
        this.items.unshift(item)
        this.view.update()
    }
}


productsView = {
    dataModel: null,
    update(){
        console.log( this.render() )
    },
    render(){
        return `Wyswietlam ${this.dataModel.items.join(', ')}`
    }
}

// https://pl.wikipedia.org/wiki/SOLID_(programowanie_obiektowe)
// - Single resp. - jedna odpowiedzialnosc / powód do zmiany
// - Open/close - otwarty na rozszerzanie (wtyczki), zamkniety na zmiany
// - Liskov subs. - wytczki mozna wyminiac na inne kompatybilne
// - Dep. inversion - szczegóły (wyświeltanie, wprowadzanie) to wtyczki

productsData.view = productsView
productsView.dataModel = productsData

// ... potem ....

productsData.addItem('Placki')